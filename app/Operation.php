<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Operation extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'operation_type', 'amount', 'total', 'user_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function operationName()
    {
        //False (0) =  entradas True(1) = salidas
        if ($this->operation_type == false) {
            return "Entrada";
        }else {
            return "Salida";
        }
    }
}
