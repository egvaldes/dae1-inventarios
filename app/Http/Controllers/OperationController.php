<?php

namespace App\Http\Controllers;

use App\Operation;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OperationController extends Controller
{
    /**
     * Display  a form for search.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        // $product = Product::find($product);
        // $operations = Operation::where('product_id', $product->id)
        //                 ->orderBy('updated_at', 'desc');
        return view('operations/operationSearch');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($filter)
    {
        // var_dump($product);
        $product = Product::find($filter);
        // var_dump($product);
        // var_dump($product->id);
        $operations = Operation::where('product_id', $product->id)
                        ->orderBy('updated_at', 'desc')
                        ->get();
        // var_dump($operations);
        // var_dump($operations);
        return view('operations/operationList', ['operations' => $operations, 'product' => $product]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('operations/operationCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::where('code',$request->input('code'))->first();
        $lastOperation = Operation::where('product_id', $product->id)
                        ->orderBy('updated_at', 'desc')
                        ->first();
        if ($lastOperation) {
            if ($request->input('selectType')==0) {
                $total = $lastOperation->total + $request->input('amount');
            }else {
                $total = $lastOperation->total - $request->input('amount');
            }
        }else {
            $total = $request->input('amount');
        }
        $operation = Operation::create([
            'product_id'        => $product->id,
            'operation_type'    => $request->input('selectType'),
            'amount'            => $request->input('amount'),
            'total'             => $total,
            'user_id'           => $request->user()->id,
        ]);
        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Operation  $operation
     * @return \Illuminate\Http\Response
     */
    public function show(Operation $operation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Operation  $operation
     * @return \Illuminate\Http\Response
     */
    public function edit(Operation $operation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Operation  $operation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Operation $operation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Operation  $operation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Operation $operation)
    {
        $operation->delete();
        return redirect()->route('dashboard');
    }
}
