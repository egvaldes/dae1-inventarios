<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('dashboard');

Route::resource('users', 'UserController');
Route::resource('products', 'ProductController');
Route::get('/operations/list/{filter}', 'OperationController@index')->name('operations.list');
Route::get('/operations/search', 'OperationController@search')->name('operations.search');
Route::get('/operations', function () {
    return redirect()->route('dashboard');
});
Route::resource('operations', 'OperationController', ['except' => ['index']]);
