@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @if ( Auth::User()->isAdmin() )
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Administrar Usuarios</h3>
                </div>
                <div class="panel-body">
                    Agregue, modifique o elimine los usuarios del sistema desde este apartado.<br>
                    <a href="{{ route( 'users.index' ) }}" class="btn btn-material-pink btn-raised pull-right">
                        <i class="material-icons">done</i>
                        <span> Ir</span>
                        <div class="ripple-container"></div>
                    </a>
                </div>
            </div>
        </div>
        @endif
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Administrar Inventarios</h3>
                </div>
                <div class="panel-body">
                    Agregue, modifique o elimine los productos registrados en sistema desde este apartado.<br>
                    <a href="{{ route( 'products.index' )}}" class="btn btn-material-pink btn-raised pull-right">
                        <i class="material-icons">done</i>
                        <span> Ir</span>
                        <div class="ripple-container"></div>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Consultar Inventario</h3>
                </div>
                <div class="panel-body">
                    Consulte existencias y datos de un producto.<br>
                    (Esta funcion no se terminó de desarrollar)<br>
                    <a href="{{ route( 'operations.search' )}}" class="btn btn-material-pink btn-raised pull-right">
                        <i class="material-icons">done</i>
                        <span> Ir</span>
                        <div class="ripple-container"></div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
