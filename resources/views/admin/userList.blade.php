@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-right">
            <a href="{{ route( 'users.create' ) }}" class="btn btn-raised btn-primary">
                Crear Usuario
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Usuarios registrados</div>

                <div class="panel-body">
                    <div class="row">
                        <table class="table table-striped table-hover ">
            				<thead>
            					<tr>
            						<th>ID</th>
            						<th>Nombre</th>
            						<th>Apellido</th>
            						<th>Email</th>
            						<th>Rol</th>
            						<th>Acciones</th>
            					</tr>
            				</thead>
            				<tbody>
                    @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->lastname }}</td>
                                <td>{{ $user->email }}</td>
                                <td>@if ($user->is_admin==0)
                                    Usuario
                                    @else
                                    Administrador
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-raised">Acciones</a>
                                        <a href="#" class="btn btn-primary btn-sm btn-raised dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ route( 'users.show',$user->id )}}">Ver</a></li>
                                            <li><a href="{{ route( 'users.edit',$user->id )}}">Editar</a></li>
                                            <li class="divider"></li>
                                            <li><a href="javascript:void(0)" onclick="$('#deleteForm-{{$user->id}}').submit()">Eliminar</a></li>
                                            <form id="deleteForm-{{$user->id}}" action="{{ route( 'users.destroy',$user->id )}}" method="post">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                            </form>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
