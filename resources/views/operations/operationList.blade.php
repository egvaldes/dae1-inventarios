@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-right">
            <a href="{{ route( 'operations.create' ) }}" class="btn btn-raised btn-primary">
                Crear Operación
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Operaciones del producto #{{$product->id}} Codigo {{$product->code}}</div>

                <div class="panel-body">
                    <div class="row">
                        <table class="table table-striped table-hover ">
            				<thead>
            					<tr>
            						<th>ID Operación</th>
            						<th>Operación</th>
            						<th>Cantidad operación</th>
            						<th>Cantidad actual</th>
            						<th>Usuario</th>
            					</tr>
            				</thead>
            				<tbody>
                    @foreach ($operations as $operation)
                            <tr>
                                <td>{{ $operation->id }}</td>
                                <td>@if ($operation->operation_type==0)
                                    Entrada
                                    @else
                                    Salida
                                    @endif
                                </td>
                                <td>{{ $operation->amount }}</td>
                                <td>{{ $operation->total }}</td>
                                <td>{{ $operation->user_id }}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-raised">Acciones</a>
                                        <a href="#" class="btn btn-primary btn-sm btn-raised dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ route( 'operations.show',$operation->id )}}">Ver</a></li>
                                            {{--<li><a href="{{ route( 'operations.edit',$operation->id )}}">Editar</a></li>
                                            <li class="divider"></li>
                                            <li><a href="javascript:void(0)" onclick="$('#deleteForm-{{$operation->id}}').submit()">Eliminar</a></li>
                                            <form id="deleteForm-{{$operation->id}}" action="{{ route( 'operations.destroy',$operation->id )}}" method="post">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                            </form>--}}
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
