@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-right">
            @php $product = 1; @endphp
            <a href="{{ route('operations.list', ['filter' => $product]) }}" class="btn btn-raised btn-primary">
                Crear Producto
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Buscar operaciones del producto</div>

                <div class="panel-body">
                    <div class="row">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
