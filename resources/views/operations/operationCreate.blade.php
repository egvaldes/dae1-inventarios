@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Nueva operación</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('operations.store') }}">
                    <fieldset>
                        {{ csrf_field() }}
                        {{ method_field('POST') }}

                        <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                            <label for="code" class="col-md-4 control-label">Código de producto</label>

                            <div class="col-md-6">
                                <input id="code" type="text" class="form-control" name="code" required autofocus>

                                @if ($errors->has('code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="selectType" class="col-md-4 control-label">Tipo de operación</label>

                            <div class="col-md-6">
                                <select id="selectType" name="selectType" class="form-control">
                                    <option value="0" selected>Entrada</option>
                                    <option value="1">Salida</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                            <label for="amount" class="col-md-4 control-label">Cantidad</label>

                            <div class="col-md-6">
                                <input id="amount" type="number" class="form-control" name="amount" required autofocus>

                                @if ($errors->has('amount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-raised btn-success">
                                    Registrar operación
                                </button>
                                <a href="{{ url()->previous() }}" class="btn btn-raised btn-danger">
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
