@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Crear Producto</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('products.store') }}">
                    <fieldset>
                        {{ csrf_field() }}
                        {{ method_field('POST') }}

                        <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                            <label for="code" class="col-md-4 control-label">Código</label>

                            <div class="col-md-6">
                                <input id="code" type="text" class="form-control" name="code" required autofocus>

                                @if ($errors->has('code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nombre</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('price_in') ? ' has-error' : '' }}">
                            <label for="price_in" class="col-md-4 control-label">Precio de entrada</label>

                            <div class="col-md-6">
                                <input id="price_in" type="text" class="form-control" name="price_in" required autofocus placeholder="$000.00">

                                @if ($errors->has('price_in'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price_in') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('price_out') ? ' has-error' : '' }}">
                            <label for="price_out" class="col-md-4 control-label">Precio de salida</label>

                            <div class="col-md-6">
                                <input id="price_out" type="text" class="form-control" name="price_out" required autofocus placeholder="$000.00">

                                @if ($errors->has('price_out'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('price_out') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('unit') ? ' has-error' : '' }}">
                            <label for="unit" class="col-md-4 control-label">Unidades</label>

                            <div class="col-md-6">
                                <input id="unit" type="text" class="form-control" name="unit" required>

                                @if ($errors->has('unit'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('unit') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('presentation') ? ' has-error' : '' }}">
                            <label for="presentation" class="col-md-4 control-label">Presentación</label>

                            <div class="col-md-6">
                                <input id="presentation" type="text" class="form-control" name="presentation" required>

                                @if ($errors->has('presentation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('presentation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-raised btn-success">
                                    Crear producto
                                </button>
                                <a href="{{ url()->previous() }}" class="btn btn-raised btn-danger">
                                    Cancelar
                                </a>
                            </div>
                        </div>
                    </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
