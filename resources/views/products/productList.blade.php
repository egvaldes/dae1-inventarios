@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-right">
            <a href="{{ route( 'products.create' ) }}" class="btn btn-raised btn-primary">
                Crear Producto
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-primary">
                <div class="panel-heading">Productos registrados</div>

                <div class="panel-body">
                    <div class="row">
                        <table class="table table-striped table-hover ">
            				<thead>
            					<tr>
            						<th>ID</th>
                                    <th>Código</th>
            						<th>Nombre</th>
            						<th>Precio de entrada</th>
            						<th>Precio de salida</th>
            						<th>Unidades</th>
            						<th>Presentación</th>
            						<th>Acciones</th>
            					</tr>
            				</thead>
            				<tbody>
                    @foreach ($products as $product)
                            <tr>
                                <td>{{ $product->id }}</td>
                                <td>{{ $product->code }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->price_in }}</td>
                                <td>{{ $product->price_out }}</td>
                                <td>{{ $product->unit }}</td>
                                <td>{{ $product->presentation }}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm btn-raised">Acciones</a>
                                        <a href="#" class="btn btn-primary btn-sm btn-raised dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ route( 'products.show',$product->id )}}">Ver</a></li>
                                            <li><a href="{{ route( 'products.edit',$product->id )}}">Editar</a></li>
                                            <li class="divider"></li>
                                            <li><a href="{{ route( 'operations.list',['filter' => $product->id] )}}">Operaciones</a></li>
                                            <li class="divider"></li>
                                            <li><a href="javascript:void(0)" onclick="$('#deleteForm-{{$product->id}}').submit()">Eliminar</a></li>
                                            <form id="deleteForm-{{$product->id}}" action="{{ route( 'products.destroy',$product->id )}}" method="post">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                            </form>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
